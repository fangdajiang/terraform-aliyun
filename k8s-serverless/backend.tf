
terraform {
  backend "oss" {
    bucket              = "terraform-elvin"
    prefix              = "terraform"
    key                 = "k8s/demo/k8s-serverless/terraform.tfstate"
    acl                 = "private"
    encrypt             = "true"
    region              = "cn-shanghai"
    profile             = "default"
  }
}

data "terraform_remote_state" "network" {
  backend = "oss" 
  config  = {
    bucket              = "terraform-elvin"
    prefix              = "terraform"
    key                 = "network/terraform.tfstate"
    acl                 = "private"
    encrypt             = "true"
    region              = "cn-shanghai"
    profile             = "default"
  }
}

