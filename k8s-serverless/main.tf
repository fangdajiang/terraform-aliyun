
# 名称
variable "name" {
  default = "elvin-k8s-serverless-demo"
}

resource "alicloud_cs_serverless_kubernetes" "serverless" {
  name = var.name
  vpc_id = data.terraform_remote_state.network.outputs.vpc.id
  vswitch_ids = [data.terraform_remote_state.network.outputs.vswitch_info.k8s.f]
  new_nat_gateway = false
  #API Server创建Internet eip
  endpoint_public_access_enabled = false
  private_zone = false
  tags = {
        "name": "${var.name}",
  }
}


output "name" {
  value       = alicloud_cs_serverless_kubernetes.serverless.*.name
}


#参考
#https://registry.terraform.io/providers/aliyun/alicloud/latest/docs/resources/cs_serverless_kubernetes
