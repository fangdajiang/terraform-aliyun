
#使用oss存储本目录资源状态文件
terraform {
  backend "oss" {
    bucket              = "terraform-elvin"
    prefix              = "terraform"
    key                 = "bastion/terraform.tfstate"
    acl                 = "private"
    encrypt             = "true"
    region              = "cn-shanghai"
    profile             = "default"
  }
}

#获取network资源信息
data "terraform_remote_state" "network" {
  backend = "oss" 
  config  = {
    bucket              = "terraform-elvin"
    prefix              = "terraform"
    key                 = "network/terraform.tfstate"
    acl                 = "private"
    encrypt             = "true"
    region              = "cn-shanghai"
    profile             = "default"
  }
}

#使用Backend状态文件,根据outputs获取值 
#＃如获取vpc.id  data.terraform_remote_state.network.outputs.vpc.id 
