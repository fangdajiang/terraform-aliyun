resource "alicloud_instance" "instance" {
  instance_type        = var.instance_type
  system_disk_size     = var.system_disk_size
  image_id             = var.image_id == "" ? data.alicloud_images.os.images.0.id : var.image_id
  availability_zone    = "${var.region}-${element(var.alicloud_availability_zones, count.index)}"
  instance_name        = "${var.region}-${element(var.alicloud_availability_zones, count.index)}-${var.environment}-${var.service}-${count.index +1}"
  host_name            = "${var.region}-${element(var.alicloud_availability_zones, count.index)}-${var.environment}-${var.service}-${count.index +1}"
  # internet_max_bandwidth_out = 0
  system_disk_category = "cloud_efficiency"
  vswitch_id           = var.vswitch_info["${element(var.alicloud_availability_zones, count.index)}"]
  count                = var.instance_count
  user_data            = file("${path.module}/scripts/setup.sh")
  security_groups      = var.security_groups
  tags                 = merge(map("service_name", var.service), map("env", var.environment), var.tags)
  # is_outdated          = true
  # period_unit          = "Month"
  # instance_charge_type = var.charge_type
  # period               = var.charge_period
  # renewal_status       = "AutoRenewal"
  password             = var.password

  # volume_tags          = 

  #抢占模式,按需付费, for test
  instance_charge_type = "PostPaid"
  spot_strategy        = "SpotWithPriceLimit"

  #忽略
  lifecycle {
    ignore_changes = [user_data,spot_price_limit]
  }
}

