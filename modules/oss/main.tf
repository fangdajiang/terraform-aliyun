
resource "alicloud_oss_bucket" "terraform-elvin" { 
    bucket = "terraform-elvin" 
    acl = "private"
    tags = {
        "Terraform" = "true"
    }
    versioning {
        status = "Enabled"
    }
}

# terraform import alicloud_oss_bucket.terraform-elvin terraform-elvin


#
#https://www.terraform.io/docs/providers/alicloud/r/oss_bucket.html
