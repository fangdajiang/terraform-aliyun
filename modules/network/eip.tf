
resource "alicloud_eip" "elvin_nat_eip" {
  name                 = "elvin_nat_eip"
  bandwidth            = 10
  internet_charge_type = "PayByTraffic"
}

resource "alicloud_eip" "elvin_bastion" {
  name                 = "elvin_bastion"
  bandwidth            = 10
  internet_charge_type = "PayByTraffic"
}


#默认PayByTraffic 按流量收费
